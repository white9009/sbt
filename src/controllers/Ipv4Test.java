package controllers;

import junit.framework.TestCase;

/**
 * 测试用例
 * @author steven
 *
 */
public class Ipv4Test extends TestCase{
    public void testCheckSum(){
    	Ipv4Project ipv4=new Ipv4Project();
        String res=ipv4.checksum("4500 0073 0000 4000 4011 0000 c0a8 0001 c0a8 00c7");
        assertEquals("4500 0073 0000 4000 4011 b861 c0a8 0001 c0a8 00c7",res);
    } 
}
