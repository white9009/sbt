package controllers;

import junit.framework.TestCase;

/**
 * 测试用例
 * @author steven
 *
 */
public class Ipv4Test1 extends TestCase{
    public void testCheckSum(){
    	Ipv4Project ipv4=new Ipv4Project();
        String res=ipv4.checksum("4500 0042 3038 0000 4011 0000 c0a8 0afa af90 6ce9");
        assertEquals("4500 0042 3038 0000 4011 6257 c0a8 0afa af90 6ce9",res);
    } 
}
