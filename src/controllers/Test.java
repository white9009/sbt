package controllers;

import junit.framework.TestSuite;
import junit.textui.TestRunner;

/**
 * 单元测试
 * @author steven
 *
 */
public class Test {

	public static TestSuite suite() {
		TestSuite suite = new TestSuite();
		suite.addTestSuite(Ipv4Test.class);
		suite.addTestSuite(Ipv4Test1.class);
		return suite;
	}

	public static void main(String args[]) {
		TestRunner.run(suite());
	}

}
