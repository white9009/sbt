package controllers
import java.lang.Integer
import java.lang.Long
import java.lang.String
import java.lang.Character

class Ipv4Project {
  def checksum(header: String): String = {
    //1.十六进制相加
    var numStr = strToDecimalDigits(header)
    //2.转成二
    var num = intToByte(numStr)
    //3。高位补0
    num = addByZero(20,num)
    //4.二进制相加
    num = sumByByte(num)
    //5.高位补0
    num = addByZero(16,num)
    //6.最后转16进制
    num = getLastResult(num)
    //7.替换掉原有的0000
    num = header.substring(0, 25) + num + header.substring(29, header.length())
    num
  }

  //16进制相加
  def strToDecimalDigits(str: String): Int = {
    var strArray = str.split(" ")
    val length = strArray.length
    var result: Int = 0
    for (i <- 0 to length - 1) {
      //转成10进制相加
      result = result + Integer.parseInt(strArray(i), 16)
    }
    result.toInt
  }

    //十进制转二进制
  def intToByte(num: Int): String = {
    Integer.toBinaryString(num)
  }
 
  //将二进制转为十六进制
  def binaryToHexString(num : String) :String = {
    Long.toHexString(Long.parseLong(num,2))
  }
  
  //高位补0
  def addByZero(len : Int,num : String) : String = {
    var needStrLength = len - num.length()
    var result = ""
    for (i <- 0 to needStrLength - 1)
      result += "0"
   result = result + num   
   result
  }
  //相加 
  def sumByByte(result: String): String = {
   var adder:String = result.substring(0, 4)
   var byteExpression:String = result.substring(4, result.length())
   var sum = ""
   intToByte(Integer.parseInt(adder, 2) + Integer.parseInt(byteExpression, 2))
  }
  
  //获取最后结果
  def getLastResult(result : String) : String = {
    var resultArr = result.toCharArray()
    var lastResult = ""
    var s = ""
    //二进制翻转
    for(i <- 0 to resultArr.length - 1){
      if (resultArr.apply(i) == '1') 
        s = "0" 
      else 
        s = "1"
      lastResult += s
    }
    binaryToHexString(lastResult)
  }


  def main(args: Array[String]) {
    println(checksum("4500 0073 0000 4000 4011 0000 c0a8 0001 c0a8 00c7"))
    println(checksum("4500 0042 3038 0000 4011 0000 c0a8 0afa af90 6ce9"))
  }
}